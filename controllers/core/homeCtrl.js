"use strict";

angular.module('mfisheries.controllers')


.controller('HomeCtrl', function($scope, fileupload, $q){

	$(".mFish_menu").removeClass("active");
	$("#menu-country").addClass("active");

	$scope.user = {};
	$scope.submit = function(){
		const temp = {
			"userid": 1,
			"file": $scope.user.file,
			"text": "something random",
			"latitude": "10.511488",
			"longitude": "-61.405772",
			"isSpecific": "yes"
		};
		const postfile = fileupload.post(temp);

		postfile.then(function(res){
			console.log(res);
		});
	};
});


// .controller('UserInfoCtrl', function($scope, $routeParams, $q){
	
// 	var modules = _.without($routeParams.modules.match(/[a-z'\-]+/gi),'modules');	
// 	$scope.modules = {};
// 	$.each(modules, function(index, data){
// 		$scope.modules[data] = data;
// 	});
// 	// console.log($scope.modules);

// });