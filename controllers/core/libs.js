'use strict';

//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
class BaseAPIService{
	/**
	 *
	 * @param $http
	 * @param url
	 */
	constructor($http, url){
		this.$http = $http;
		this.base_url = url;
		this.debug = true;
	}
	
	/**
	 *
	 * @param countryid
	 */
	get (countryid) {
		const options = {};
		if (countryid)options.countryid = countryid;
		return this.$http.get(this.base_url, {params:options});
	}
	
	/**
	 *
	 * @param id
	 */
	getById(id){
		return this.$http.get(this.base_url +"/"+id);
	}
	
	/**
	 *
	 * @param data
	 */
	add(data){
		return this.$http.post(this.base_url, data);
	}
	
	save(data){
		return this.add(data);
	}
	
	/**
	 *
	 * @param source
	 */
	update(source) {
		return this.$http.put(this.base_url + "/" + source.id, source);
	}
	
	// noinspection ReservedWordAsName
	/**
	 *
	 * @param source
	 */
	delete(source){
		return this.$http.delete(this.base_url+ "/" + source.id);
	}
}

// https://medium.com/@yuribett/javascript-abstract-method-with-es6-5dbea4b00027
class BaseController{
	constructor(Country, Locator, AuthService){
		console.log("Constructor of BaseController was initialized");
		this.Country = Country;
		this.locator = Locator;
		this.AuthService = AuthService;
		this.infoName = "BaseController";
		
		this.primaryModal = "#modelModal";
	}
	
	init(){
		this.countries = [];
		// Check if user is signed in
		this.AuthService.attachCurrUser(this).then(currUser => {
			if (currUser)console.log("User is logged in");
			else console.log("User is not logged in");
		});
	}

	retrieveCountries(display) {
		return new Promise((resolve, reject) => {
			const self = this;
			this.Country.get(self.userCountry).then(res => {
				self.countries = res.data;
				if (self.debug)console.log("Retrieved %s countries", res.data.length);
				if (display) self.countries.unshift({ id: 0, name: "Select Country"});
				resolve(self.countries);
			}, reject);
		});
	}

	startView(){
		return new Promise((resolve, reject) => {
			console.log("starting Alert Public Tasks");
			this.displayLoading("Loading Countries").then(loading => {
				this.retrieveCountries(true).then(countries => {
					loading.close();
					this.locator.getLastLocationID().then(countryid => {
						console.log("Location found from cache as: " + countryid);
						if (countryid && countryid !== "undefined")this.handleCountryChange(countryid);
						resolve(countries);
					});
				}, err => { loading.close(); reject(err); });
			});
		});

	}

	/**
	 * Displays the a control-less loading message dialog.
	 * Clients that use the function should use the extent and call close
	 * method on the response to dismiss message
	 * @param message
	 * @returns {Promise}
	 */
	displayLoading(message){
		swal({
			type: "info",
			title: this.infoName ,
			text: message,
			allowOutsideClick: false,
			allowEscapeKey: false,
			showConfirmButton: false,
			onOpen: function () {
				swal.showLoading();
			}
		});

		return Promise.resolve(swal);
	}
	
	displayAdd(){
		console.log("Launching modal to add new record");
		this.reset();
		const modelModal = $(this.primaryModal);
		modelModal.modal('show');
		// modelModal.updatePolyfill();
	}
	
	reset(){
		throw new Error('You have to implement the method doSomething!');
	}

}