"use strict";

angular.module('mfisheries.Country', [])
	.controller('adminCountryCtrl', [
		'$scope', 'LocalStorage', 'fileupload', 'Country', 'Module', 'CountryModule', 'CountryLoc', 'AuthService',
		function ($scope, LocalStorage, fileupload, Country, Module, CountryModule, CountryLoc, AuthService) {
			
			$(".mFish_menu").removeClass("active");
			$("#menu-country").addClass("active");
			
			
			$scope.countries = [];
			$scope.modules = [];
			$scope.pathLessModules = [];
			$scope.countrymodules = [];
			$scope.editCountryModule = {};
			$scope.module = {};
			$scope.country = {};
			$scope.modules_loading = true;
			$scope.country_loading = true;
			$scope.countryModule_loading = true;

			// additional variables for ACL
			$scope.readOnly = true;
			$scope.userCountry = -1; // Default will be set for -1

			// Configure operations for ACL
			AuthService.attachCurrUser($scope);
			const currUser = $scope.currentUser;
			
			// Utilities functionality

			// gets the template to ng-include for a table row / item
			$scope.getTemplate = function (country) {
				if (country.id === $scope.country.id) return 'edit';
				else return 'display';
			};
			
			$scope.getModuleTemplate = function (module) {
				if (module.id === $scope.module.id) return 'medit';
				else return 'mdisplay';
			};
			
			// Country Functionality
			
			function loadCountries() {
				$scope.country_loading = true;
				Country.get($scope.userCountry).then(res => {
					$scope.countries = res.data;
					$scope.country_loading = false;
				}, err => {
					$scope.country_loading = false;
					console.error("Unable to retrieve countries: " + err);
				});
			}
			
			$scope.editCountry = function (country) {
				$scope.country = country;
			};
			
			$scope.addCountry = function (country) {
				$("#countryModal").modal('show');
			};
			
			$scope.reset = function () {
				$scope.country = {};
			};
			
			$scope.saveCountry = function (country) {
				swal({
					title: "Update Confirmation",
					text: "Are you sure you want to update this record. You will not be able to undo this operation",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#3e8f3e",
					confirmButtonText: "Update",
					closeOnConfirm: false
				}).then(res => {
					if (res.value) {
						Country.update(country).then(() => {
							swal("Updated", "Record was successfully updated", "success");
						}, err => {
							swal("Failed", "Unable to Update record", "error");
							console.log(err);
						});
						$scope.country = {};
					}
				});
			};
			
			$scope.deleteCountry = function (country) {
				swal({
					title: "Delete Confirmation",
					text: "Are you sure you want to delete this record. You will not be able to undo this operation",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete",
					closeOnConfirm: false
				}).then(res => {
					if (res.value) {
						Country.delete(country).then(res => {
							swal("Deleted", "Record was successfully deleted", "success");
							$scope.countries.splice(
								_.indexOf($scope.countries, _.find($scope.countries, cntry => cntry.id === country.id)), 1);
						}, err => {
							console.log(err);
							swal("Failed", "Unable to delete record", "error");
						});
					}
				});
			};
			
			$scope.submit = function () {
				if (!$scope.country.path) {
					$scope.country.path = `static\country_modules\${$scope.country.name}`;
				}
				console.log("Country path is " + $scope.country.path);
				
				Country
					.add($scope.country)
					.then(() => {
						loadCountries();
						swal("Add Country", "Country was Added Successfully", "success");
						$("#countryModal").modal('hide').on('hidden', $scope.reset);
					}, () => {
						swal("Creating Country", "Error creating country!", "error");
						$("#countryModal").modal('hide').on('hidden', $scope.reset);
					});
			};
			
			// initialization
			
			loadCountries();
			loadModules();
			loadCountryModules();

			// Module Functionality
			
			function loadModules() {
				$scope.modules_loading = true;
				Module.get().then(res => {
					$scope.modules = res.data;
					$scope.modules_loading = false;
					console.log("Received %s modules", $scope.modules.length);
					$scope.pathLessModules = $scope.modules.map(el => {
						if (el.hasDownload === 'no')
							return el;
					});
				}, err =>{
					$scope.modules_loading = false;
					console.error(err);
				});
			}
			
			$scope.submitModule = function () {
				$scope.module.path = $scope.module.name;
				$scope.module.hasDownload = "no";
				$scope.module.description = "";
				
				console.log($scope.module);
				
				if ($scope.modules.filter(el => el.name === $scope.module.name).length > 0){
					swal("Module", "Unable to create Module. Module already exists.", "error");
				}else{
					Module.add($scope.module).then(res => {
						loadModules();
						swal("Add Module", "Module was added successfully", "success");
						$("#moduleModal").modal('hide');
						$scope.resetModule();
					}, err => {
						swal(
							"Module",
							"Error creating module. If problem persists, contact system administrator.",
							"error"
						);
						$("#moduleModal").modal('hide');
						$scope.resetModule();
						console.log(err);
					});
				}
			};
			
			$scope.deleteModule = function (module) {
				console.log("Delete Module selected");
				swal({
					title: "Delete Confirmation",
					text: "Are you sure you want to delete this record. You will not be able to undo this operation",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete",
					closeOnConfirm: false
				}).then(res => {
					if (res.value) {
						Module.delete(module).then(res => {
							swal("Deleted", "Record was successfully deleted", "success");
							
							$scope.modules.splice(
								_.indexOf($scope.modules, _.find($scope.modules, function (mdl) {
									return mdl.id === module.id;
								})), 1);
							
							$scope.pathLessModules.splice(
								_.indexOf($scope.pathLessModules, _.find($scope.pathLessModules, function (mdl) {
									return mdl.id === module.id;
								})), 1);
							
						}, err => {
							swal("Failed", "Unable to delete record", "error");
							console.log(err);
						});
					}
				});
			};
			
			$scope.saveModule = function (module) {
				swal({
					title: "Update Confirmation",
					text: "Are you sure you want to update this record. You will not be able to undo this operation",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#3e8f3e",
					confirmButtonText: "Update",
					closeOnConfirm: false
				}).then( res => {
					if (res.value) {
						Module.update(module).then(res => {
							swal("Updated", "Record was successfully updated", "success");
							console.log(res);
						}, err => {
							swal("Failed", "Unable to Update record", "error");
							console.log(err);
						});
						$scope.resetModule();
					}
				});
			};
			
			$scope.editModule = function (module) {
				$scope.module = module;
			};
			
			$scope.resetModule = function () {
				$scope.module = {};
			};
			
			// Country Module Functionality
			
			function loadCountryModules() {
				console.log($scope.userCountry);
				$scope.countryModule_loading = true;
				CountryModule.get($scope.userCountry).then(res => {
					$scope.countrymodules = res.data;
					console.log("Received " + $scope.countrymodules.length + " Country Modules");
					console.log($scope.countrymodules);
					$scope.countryModule_loading = false;

				}, err => {
					console.log("No country modules received:");
					console.log(err);
					$scope.countryModule_loading = false;
				});
			}
			
			// Deleting country modules
			$scope.deleteCModule = function (cmodule) {
				console.log("Attempting to delete country module with id: " + cmodule.id);
				swal({
					title: "Delete Confirmation",
					text: "Are you sure you want to delete this record. You will not be able to undo this operation",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete",
					closeOnConfirm: false
				}).then( res => {
					if (res.value) {
						CountryModule.delete(cmodule).then(res => {
							swal("Deleted", "Record was successfully deleted", "success");
							$scope.countrymodules.splice(
								_.indexOf($scope.countrymodules, _.find($scope.countrymodules, function (countrymodule) {
									return countrymodule.id === cmodule.id;
								})), 1);
						}, err => {
							swal("Failed", "Unable to delete record", "error");
							console.log(err);
						});
					}
				});
			};
			
			$scope.openUploadCModule = function (cmodule) {
				$scope.cmodule = cmodule;
				$("#countryModuleUploadModal").modal('show');
			};
			
			$scope.submitUploadModule = function (cmodule) {
				const file = $scope.myFile;
				const uploadUrl = '/api/add/modulefile';
				const sendData = {
					countryid: cmodule.countryid,
					moduleid: cmodule.moduleid
				};
				if (file) {
					$("#spinning_upload").show();
					fileupload.uploadFileToUrl(file, sendData, uploadUrl).then(function (data) {
						if (data.data.status === 200) {
							swal({
								title: "Uploaded Module",
								type: "success",
								text: "File was successfully uploaded"
							}).then( res => {
								if (res.value) {
									loadModules();
									loadCountryModules();
								}
							});
						} else {
							swal({
								title: "Uploading Module",
								type: "error",
								text: "Unable to Upload File"
							}).then(() => {
								console.log("Error Occurred when attempting to upload file: ");
								console.error(data);
							});
						}
						$("#countryModuleUploadModal").modal('hide');
						delete $scope.cmodule;
					});
				} else {
					swal("Uploading Module", "Please select File Before uploading", "error");
				}
				
			};

			$scope.submitCountryModule = function () {
				$scope.editCountryModule.path = "";
				console.log($scope.editCountryModule);
				CountryModule.add($scope.editCountryModule).then(res => {
					loadModules();
					loadCountryModules();
					swal("Add Country Module", "Country Module was Added Successfully", "success");
					$("#countryModuleModal").modal('hide');
					$scope.resetCountryModule();
				}, err => {
					swal("Add Country Module", "Error creating country module!", "error");
					$("#countryModuleModal").modal('hide');
					$scope.resetCountryModule();
				});
			};
			
			$scope.resetCountryModule = function () {
				$scope.editCountryModule = {
					countryid: currUser.countryid
				};
			};

			// Weather
			$scope.addSource = function () {
				$("#weatherModal").modal('show');
			};
			
			$scope.newType = function () {
				$("newType").removeClass("hidden");
			};

			// Country Location Functionality
			$scope.countryLocations = [];
			$scope.country_location_loading = true;
			$scope.countryLoc = {};
			
			loadCountryLocations();
			resetCountryLoc();
			
			function resetCountryLoc(){
				$scope.countryLoc = {
					countryid: currUser.countryid,
					createdby: currUser.userid
				};
			}
			
			function loadCountryLocations() {
				$scope.country_location_loading = true;
				console.log("Attempting  to load country locations");
				CountryLoc.get($scope.userCountry).then(res => {
					$scope.country_location_loading = false;
					$scope.countryLocations = res.data;
				}, err => { console.log(err);	});
			}
			
			$scope.displayAddCountryLocation = function () {
				console.log("Add Country Location selected");
				resetCountryLoc();
				$("#countryLocationModal").show();
			};
			
			$scope.submitCountryLoc = function(countryLoc){
				console.log("submitting " + JSON.stringify(countryLoc));
				if (countryLoc.id){
					swal({
						title: "Update Confirmation",
						text: "Are you sure you want to update this record. You will not be able to undo this operation",
						type: "warning",
						showCancelButton: true,
						confirmButtonText: "Update",
						closeOnConfirm: false
					}).then( res => {
						if (res.value) {
							CountryLoc.update(countryLoc).then(res => {
								swal("Country Location", "Record was successfully updated", "success");
							}, () => {
								swal("Country Location", "Unable to update record", "error");
							});
						}
					});
				}else{
					CountryLoc.add(countryLoc).then(res =>{
						loadCountryLocations();
						swal("Country Location","Country Location was successfully created","success");
					}, () => { swal("Country Location", "Unable to update record", "error"); });
				}
				resetCountryLoc();
			};
			
			$scope.deleteCountryLoc = function(countryLoc){
				swal({
					title: "Delete Confirmation",
					text: "Are you sure you want to delete this record. You will not be able to undo this operation",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete",
					closeOnConfirm: false
				}).then( res => {
					if (res.value) {
						CountryLoc.delete(countryLoc).then(function (res) {
							swal("Country Location", "Record was successfully deleted", "success");
							loadCountryLocations();
						}, () => {
							swal("Country Location", "Unable to delete record", "error");
						});
					}
				});
				resetCountryLoc();
			};
		}]);