"use strict";

angular.module('mfisheries.Occupation', [])
	
	.controller('adminOccupationCtrl', ["$scope", "LocalStorage", "Occupations", "Country", 'AuthService',
		function ($scope, LocalStorage, Occupations, Country, AuthService) {
			
			$(".mFish_menu").removeClass("active");
			$("#menu-occupations").addClass("active");
            $("#menu-meta").addClass("active");
			
			$scope.occupations = [];
			$scope.countries = [];
			$scope.readOnly = true;
			
			AuthService.attachCurrUser($scope);
			
			function getOccupations() {
				Occupations.get($scope.userCountry).then(function (res) {
					$scope.occupations = res.data;
				});
			}
			
			function getCountries() {
				Country.get($scope.userCountry).then(function (res) {
					$scope.countries = res.data;
				});
			}
			
			function reset() {
				$scope.occupation = {
					countryid: $scope.currentUser.countryid
				};
			}
			
			getOccupations();// Make Request for Occupations when the Occupation Page is requested
			getCountries();
			reset();
			
			
			$scope.edit = function (occupation) {
				$scope.occupation = occupation;
			};
			
			$scope.delete = function (occupation) {
				swal({
					title: "Delete Confirmation",
					text: "Are you sure you want to delete this record. You will not be able to undo this operation",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete",
					closeOnConfirm: false
				}).then(res => {
					console.log(res);
					if (res.value) {
						Occupations.delete(occupation).then(function (res) {
							if (res && res.status === 200) {
								swal("Deleted", "Record was successfully deleted", "success");
								getOccupations();
							}
							else swal("Failed", "Unable to delete record", "error");
						});
					}
				});
			};
			
			$scope.displayUpdate = function (occupation) {
				$scope.occupation = occupation;
				$('#occupationModal').modal('show');
			};
			
			$scope.save = function (occupation) {
				console.log(occupation);
				if (occupation.id && occupation.id > 0) {
					Occupations.update(occupation).then(() => {
						swal("Update Occupation", "Occupation was successfully Updated", "success");
						$('#occupationModal').modal('hide');
						getOccupations();
					}, (err) => {
						console.log(err);
						swal("Update Occupation", "Unable to update occupation. If error persist contact administrator for support.", "error");
					});
				} else {
					// Filter to determine if occupation exists
					let sim = $scope.occupations.filter((el) => {
						return el.countryid === occupation.countryid && el.type === occupation.type;
					});
					if (sim.length < 1) {
						Occupations.add(occupation).then((res) => {
							if (res.status === 201) {
								swal("Add Occupation", "Occupation was successfully Saved", "success");
								$('#occupationModal').modal('hide');
								getOccupations();
							} else {
								swal("Add Occupation", "Unable to save occupation", "error");
							}
						}, (err) => {
							swal("Add Occupation", "Unable to save occupation. If error persist contact administrator for support.", "error");
							console.log(err);
						});
					} else {
						swal("Adding New Occupation", "Unable to add new Occupation. Occupation exists.", "error");
					}
				}
			};
			
			$scope.add = function () {
				$("#occupationModal").modal('show');
			};
			
			$scope.submit = function (occupation) {
				// Filter to determine if occupation exists
				let sim = $scope.occupations.filter((el) => {
					return el.countryid === occupation.countryid && el.type === occupation.type;
				});
				// if it does not exist attempt to add through the service
				if (sim.length < 1) {
					Occupations.add(occupation).then((res) => {
						if (res.data && res.data && res.data.status === 201) {
							swal({
								title: "Adding New Occupation",
								text: "Occupation was successfully created",
								type: "success"
							}).then( res => {
								$("#occupationModal").modal('hide');
								window.location.reload();
							});
						} else
							swal("Adding New Occupation", "Unable to add new Occupation! If problem persists, contact administrator for assistance", "error");
					});
				} else {
					swal("Adding New Occupation", "Unable to add new Occupation. Occupation exists.", "error");
				}
				
			};
			
			// gets the template to ng-include for a table row / item
			$scope.getTemplate = function (occupation) {
				if ($scope.occupation && $scope.occupation.id === occupation.id) return 'edit';
				return 'display';
			};
			
		}]);