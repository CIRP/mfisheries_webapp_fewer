"use strict";

angular.module('mfisheries.User', [])
		.controller('adminUserCtrl', [
			'$scope', 'LocalStorage', 'Country', 'User', 'USER_ROLES', 'AuthService',
			function ($scope, LocalStorage, Country, User, USER_ROLES, AuthService) {

				$(".mFish_menu").removeClass("active");
				$("#menu-user").addClass("active");

				$scope.users = [];
				$scope.userRoles = {};
				$scope.editedUser = {};

				$scope.countries = [];
				$scope.loading = true;

				// additional variables for ACL
				$scope.readOnly = true;
				$scope.userCountry = -1; // Default will be set for -1

				// Configure operations for ACL
				AuthService.attachCurrUser($scope);
				console.log($scope.role.scope.privilege);
				console.log($scope.userRoles);
				
				if ($scope.role.scope.visibility !== 'global') {
					for (let role in USER_ROLES) {
						if (USER_ROLES.hasOwnProperty(role))
							if (USER_ROLES[role].scope.visibility !== 'global')
								$scope.userRoles[role] = USER_ROLES[role];
					}
				}else $scope.userRoles = USER_ROLES;
				
				function loadUsers() {
					$scope.loading = true;
					User.get($scope.userCountry).then(function (res) {
						$scope.users = res.data.data;
						$scope.loading = false;
					});
				}

				// Load Users when the Controller Starts
				loadUsers();

				function refresh(){
					loadUsers();
				}

				Country.get($scope.userCountry).then(function (res) {
					//console.log(res.data.data.length + " country records received");
					$scope.countries = res.data;
				});

				// gets the template to ng-include for a table row / item
				$scope.getTemplate = function (user) {
					if (user.id === $scope.editedUser.id) return 'edit';
					else return 'display';
				};

				$scope.editUser = function (user) {
					// console.log(user);
					// user.countryid = _.find($scope.countries, function (country) {
					// 	return country.name === user.name;
					// }).id;
					$scope.editedUser = angular.copy(user);
				};

				$scope.getPrivilege = function (code) {
					for (let r in USER_ROLES) {
						if (USER_ROLES.hasOwnProperty(r) && USER_ROLES[r].code === code) {
							return USER_ROLES[r].name;
						}
					}
				};

				$scope.addUser = function () {
					$("#userModal").modal('show');
				};


				$scope.deleteUser = function (user, idx) {
					//TODO Message User for Confirmation before delete user
					swal({
						title: "Delete Confirmation",
						text: "Are you sure you want to delete this User. You will not be able to undo this operation",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Delete",
						closeOnConfirm: false
					}).then( res => {
						if (res.value) {
							User.delete(user).then(function (res) {
								// console.log(res.data);
								//TODO Check if operation was successful
								if (res && res.data.status === 200) {
									swal("Deleted", "Record was successfully deleted", "success");
									$scope.users.splice(idx, 1);
								} else
									swal("Failed", "Unable to delete record", "error");
							});
						}
					});
				};

				$scope.submit = function () {
					// console.log($scope.editedUser);
					if ($scope.editedUser.mobileNum === null) {
						$scope.editedUser.mobileNum = "";
					}
					let sim = $scope.users.filter((eu) => {
						return eu.username === $scope.editedUser.username;
					});

					if (sim.length < 1) {

						let dup = $scope.users.filter((eu) => {
							return eu.email === $scope.editedUser.email;
						});

						if (dup.length < 1) {

							if ($scope.editedUser.homeNum.search(/[^0-9]/) !== -1 || $scope.editedUser.mobileNum.search(/[^0-9]/) !== -1) {
								swal("Add New User", "Error Only numbers allowed in contact fields!", "error");
							}
							// console.log($scope.editedUser);
							if ($scope.editedUser.password === $scope.confirmPassword) {
								$scope.editedUser.homeNum = $scope.filterNum($scope.editedUser.homeNum);
								$scope.editedUser.mobileNum = $scope.filterNum($scope.editedUser.mobileNum);
								User.add($scope.editedUser).then(function (res) {
									if (res.data.status === 201) {
										// $scope.users.push(res.data.data);
										swal({
											title: "Add New User",
											text: "User was successfully created",
											type: "success"
										}).then(function () {
											$("#userModal").modal('hide');
											$scope.newuser.$setPristine();
											loadUsers(); // Reload Users from the Database
											$scope.reset();
											refresh();
										});
									} else {
										swal("Add New User", "Error creating user!", "error");
									}
								});
							} else {
								swal("Add New User", "Passwords Do not Match", "error");
							}
						} else {
							swal("Add New User", "Error Duplicate email", "error");
						}
					} else {
						swal("Add New User", "Error Duplicate username", "error");
					}
				};

				$scope.saveUser = function (idx) {
					// console.log("Saving User");
					let c = _.find($scope.countries, function (country) {
						return country.id === parseInt($scope.editedUser.countryid);
					});
					$scope.editedUser.name = c.name;

					$scope.users[idx] = angular.copy($scope.editedUser);
					User.update($scope.users[idx]).then(function (res) {
						console.log(res.data);
					});
					$scope.reset();
				};

				$scope.reset = function () {
					$scope.editedUser = {
						"_class": $scope.userRoles.user.code,
						"countryid": $scope.currentUser.countryid,
						"createdby": $scope.currentUser.userid
					};
					$scope.confirmPassword = "";
				};

				// Run after assignment to initialize on start
				$scope.reset();

				$scope.filterNum = function (tel) {
					// console.log(tel);
					if (!tel) {
						return '';
					}

					const value = tel.toString().replace(/[^0-9]/g, '');
					// var value = tel.toString().trim().replace(/^\+/, '');

					if (value.match(/[^0-9]/)) {
						return tel;
					}

					let city, number;
					city = value.slice(0, 3);
					number = value.slice(3);

					if (number) {
						if (number.length > 4) {
							number = number.slice(0, 3) + '-' + number.slice(3, 7);
						}
					}
					// console.log(city + "-" + number);
					return city + "-" + number;


				};

				// Added as a means of modifying the password
				$scope.changePassword = function (user) {
					$("#changePwdModal").modal('show');
				};

				$scope.submitPwd = function () {
					// console.log($scope.editedUser);
					if ($scope.editedUser.password === $scope.confirmPass) {
						// Add a signal for system to know the password changes
						$scope.editedUser.passChanged = true;

						// Submit information to the server
						User.update($scope.editedUser).then(function (res) {
							// console.log(res);
							if (res.data.status === 200) {
								swal("Changing Password", "Password Successfully Updated", "success");
								$("#changePwdModal").modal('hide');
								//update the existing user selected with the new password
								if (res.data.data) {
									$scope.editedUser.password = res.data.data.password;
									// console.log("User Model password updated");
								}
							} else {
								swal("Changing Password", "Unable to change password", "error");
							}
						});
					} else {
						swal("Changing Password", "Passwords Do not Match", "error");
					}
				};

				$("#userModal").on('hidden.bs.modal', $scope.reset);
			}]);