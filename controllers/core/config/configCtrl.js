"use strict";

angular.module('mfisheries.Config',[])
	.controller('configCtrl', [
		'$scope', 'LocalStorage', 'AuthService', 'Config',
		function ($scope, LocalStorage, AuthService, Config){

            $(".mFish_menu").removeClass("active");
            $("#menu-config").addClass("active");
            $("#menu-meta").addClass("active");
			
			AuthService.attachCurrUser($scope);
			const currUser = $scope.currentUser;
		
			$scope.configs = [];
			reset();
			retrieveConfiguration();
			
			function reset(){
				$scope.config = {
					"key": "",
					"value": "",
					"description": "",
					"createdby": currUser.userid
				};
				return $scope.config;
			}
			
			function retrieveConfiguration(){
				Config.get().then((res) => {
					$scope.configs = res.data;
					console.log($scope.configs);
					console.log("Received " + $scope.configs.length + " configurations");
				}, (el) => {
					console.log(el);
				});
			}
			
			$scope.displayAdd = function () {
				reset();
				$('#newConfigModal').modal('show');
			};
			
			$scope.displayUpdate = function (config) {
				$scope.config = config;
				$('#newConfigModal').modal('show');
			};
			
			
			$scope.save = function (config) {
				console.log(config);
				if (config.id && config.id > 0) {
					Config.update(config).then(() => {
						swal("Update Configuration", "Configuration was successfully Updated", "success");
						$('#newConfigModal').modal('hide');
						retrieveConfiguration();
					}, (err) => {
						swal("Update Configuration", "Unable to update configuration. If error persist contact administrator for support.", "error");
						console.log(err);
					});
				} else {
					Config.add(config).then((res) => {
						if (res.status === 201) {
							swal("Add Configuration", "Configuration was successfully Saved", "success");
							$('#newConfigModal').modal('hide');
							retrieveConfiguration();
						} else {
							swal("Add Configuration", "Unable to save configuration", "error");
						}
					}, (err) => {
						swal("Add Configuration", "Unable to save configuration. If error persist contact administrator for support.", "error");
						console.log(err);
					});
				}
			};
			
			$scope.delete = function (config) {
				console.log("Selected delete for: " + config.key);
				swal({
					title: "Delete Confirmation",
					text: "Are you sure you want to delete this record. You will not be able to undo this operation",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete",
					closeOnConfirm: false
				}).then(res => {
					if (res.value) {
						Config.delete(config).then(() => {
							swal("Delete Configuration", "Configuration was successfully deleted", "success");
						}, (err) => {
							swal("Delete Configuration", "Unable to delete configuration. If error persist contact administrator for support.", "error");
							console.log(err);
						});
					}
				});
				
			};
		}]);

