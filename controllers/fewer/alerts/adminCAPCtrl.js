"use strict";

angular.module('mfisheries.CAP', [])
	.controller('adminCAPCtrl', [
		'$scope',
		'LocalStorage',
		'$anchorScroll',
		'$location',
		'$routeParams',
		'CapService',
		'Alerts',
		'Country',
		'AuthService',
		'CAPSource',
		function ($scope, LocalStorage, $anchorScroll, $location, $routeParams, CapService, Alerts, Country, AuthService, CAPSource) {
			
			
			const config = CapService.getConfig();
			const COORDINATE_RADIUS = 15;
			
			let areaTemplates = [];
			let messageTemplates = [];
			const circles = [];
			const polys = [];
			const features = [];
			const featureTypes = [];
			
			
			$(".mFish_menu").removeClass("active");
			$("#menu-fewer").addClass("active");
			
			AuthService.attachCurrUser($scope);
			console.log($scope.role.scope);
			
			function init() {
				$scope.readOnly = false;
				$scope.cap_loading = true;
				$scope.alerts = [];
				$scope.communityAlerts = [];
				$scope.areaTemplateNames = [];
				$scope.messageTemplateNames = [];
				$scope.coords = [];
				$scope.alert = '';
				$scope.info = '';
				$scope.area = '';
				$scope.showAlerts = true;
				$scope.otherSelected = false;
				$scope.sending = false;
				$scope.mode = "None";
				$scope.action = "New";
			}
			
			function loadCountries(display) {
				$scope.country_loading = true;
				Country.get($scope.userCountry).then(function (res) {
					$scope.countries = res.data;
					// Attempting to Add a new Record at the beginning to give useful info for user
					if (display)
						$scope.countries.unshift({
							id: 0, name: "Display All Countries"
						});
					$scope.country_loading = false;
				});
				
			}
			
			function createAlert() {
				$scope.alert = new Alert();
				$scope.info = $scope.alert.addInfo();
				$scope.area = $scope.info.addArea();
				$scope.alert.sent = new Date().toISOString();
				$scope.alert.identifier = 'pending';
				$scope.alert.language = "en-us";
				$scope.alert.sender = 'unverified';
				
				$scope.info.senderName = $scope.currentUser.fname + " " + $scope.currentUser.lname;
				$scope.info.audience = $scope.currentUser.country;
			}
			
			function loadAlerts(countryid) {
				$scope.cap_loading = true;
				$scope.alerts = [];
				$scope.communityAlerts = [];
				// Request each country (if countryid not specified, we will load all countries)
				Country.get(countryid).then(res => {
					// Ensure we retrieve country values
					// For each country, we receive the CAP source associated with that country
					res.data.forEach(country => {
						// We retrieve the compatible CAP Source //TODO Implement logic that can accomodate all sources
						CAPSource.get(country.id, true).then(srcRes => {
							// For each source, we extract its URL, Pass it to the CAP service
							srcRes.data.forEach(srcEl => {
								CapService.setBaseURL(srcEl.url);
								// CAP service receives the alert based on information passed
								CapService.getAlerts().then(function (servRes) {
									$scope.alerts = $scope.alerts.concat(servRes.data.map(el => {
										// add the country
										console.log(servRes);
										el.country = country.name;
										return el;
									}));
									$scope.cap_loading = false; // Stop loading as soon as first is retrieved and displayed
								});
							});
						});
					});

				});
				// TODO Restructuring of alerts required
				Alerts.get().then((res) => {
					console.log("Received from alerts");
					console.log(res);
					$scope.communityAlerts = $scope.communityAlerts.concat(res.data);
				});
			}
			
			function loadTemplates() {
				CapService.getMessageTemplates().then(function (res) {
					const templatesXML = res.data;
					const names = [];
					messageTemplates = [];
					for (let i = 0; i < templatesXML.length; i++) {
						names.push(templatesXML[i].title);
						const tmpl = parseTemplateToAlert(templatesXML[i].content);
						messageTemplates.push(tmpl);
					}
					$scope.messageTemplateNames = names;
				});
				
				CapService.getAreaTemplates().then(function (res) {
					const templatesXML = res.data;
					const names = [];
					areaTemplates = [];
					for (let i = 0; i < templatesXML.length; i++) {
						names.push(templatesXML[i].title);
						const tmpl = parseTemplateToAlert(templatesXML[i].content);
						areaTemplates.push(tmpl);
					}
					$scope.areaTemplateNames = names;
				});
			}
			
			function loadSendableSources(countryid) {
				console.log("Attempting to retrieve sendable sources");
				if (!countryid) countryid = $scope.userCountry;
				CAPSource.get(countryid, true).then(res => {
					if (res.data.length > 0){ // We have a source that can be used to send data
						CapService.setBaseURL(res.data[0].url);
						loadTemplates(); // After getting the URL We will update the area and message templates
					}else{
						swal("Source", "No Internal CAP Source Configured", "error");
					}
				});
			}
			
			$scope.handleCountryChange = function (countryid, action) {
				if ($scope.countryid !== countryid) $scope.countryid = countryid;
				if (action === "list") loadAlerts(countryid);
				else if (action === "add") loadSendableSources(countryid);
			};
			
			$scope.initializeListing = function () {
				init();
				loadCountries(true);
				createAlert();
				loadAlerts();
			};
			
			$scope.refresh = function(){
				console.log("Refresh for CAP Alerts requested");
			};
			
			$scope.initializeCreation = function () {
				init();
				loadCountries(false);
				createAlert();
				loadTemplates();
				loadSendableSources($scope.currentUser.countryid);
				$scope.countryid = $scope.currentUser.countryid;
				// Set Form to print
				$(".form-control").change(() => {
					console.log($scope.alert);
				});
			};
			
			if ($routeParams.operation && $routeParams.alertid) {
				console.log("Attempting to perform operation on alert");
				loadOperationOnAlert($routeParams.operation, $routeParams.alertid);
			}
			
			function loadOperationOnAlert(operation, alertid) {
				console.log("Operation: %s - Alert ID: %s", operation, alertid);
			}
			
			$scope.updateAlert = function (alert) {
				$scope.action = "Update";
				$scope.showAlerts = false;
				$scope.alert = alert;
				$scope.alert.references = alert.sender + "," + alert.identifier + "," + alert.sent;
				$scope.alert.msgType = 'Update';
				alertToDisplay(alert);
			};
			
			$scope.cancelAlert = function (alert) {
				// console.log(alert);

				$scope.action = "Cancel";
				$scope.showAlerts = false;
				$scope.alert = alert;
				$scope.alert.references = alert.sender + "," + alert.identifier + "," + alert.sent;
				$scope.alert.msgType = "Cancel";
				alertToDisplay(alert);
			};
			
			$scope.viewAlertDetails = function(alert){
				console.log("Made request to view the alert");
			};
			
			$scope.broadcastAlert = function (alert, idx) {
				$scope.alert.msgType = "Alert";
				$scope.alert.identifier = 'pending';
				$scope.alert.language = "en-us";
				if (alert.username && alert.userAgent.length > 0){
					$scope.alert.sender = alert.username;
					$scope.info.senderName = alert.username;
				}else{
					$scope.info.senderName = $scope.alert.sender;
				}
				
				$scope.info.audience = $scope.currentUser.country;
				
				$scope.coords.push({"lat": alert.latitude, "lng": alert.longitude});
				
				$scope.info.event = alert.messagecontent;
				$scope.info.headline = alert.messagecontent;
				
				$scope.showAlerts = false;
			};
			
			$scope.ignoreAlert = function (alert, idx) {
				console.log("Ignore");
				alert.isPublic = false;
				$scope.communityAlerts.splice(idx, 1);
				Alerts.update(alert).then(function (res) {
					console.log("Updated");
				});
			};
			$scope.alertdata = {};
			
			$scope.displayRawAlert = function (alert) {
				console.log("Display ");
				console.log(alert);
				$scope.alertdata =alert;
				$("#alertsModal").modal("show");
			};
			
			$scope.newAlert = function () {
				$scope.showAlerts = false;
				$scope.action = "New";
			};
			
			function alertToDisplay(alert) {
				console.log(alert.certainty);
				$scope.info.certainty = alert.certainty;
				$scope.info.description = alert.description;
				$scope.info.event = alert.event;
				$scope.info.instruction = alert.instruction;
				$scope.info.urgency = alert.urgency;
				$scope.info.severity = alert.severity;
				$scope.info.headline = alert.title;
				$scope.info.web = alert.web;
				console.log("HERE");
				console.log($scope.info);
				$scope.info.addResponseType(alert.response_type);
				
				$scope.info.parameters = $.map(alert.parameters, function (val, i) {
					return {"valueName": val.name, "value": val.value};
				});
				
				$scope.info.addCategory(alert.category);
				$scope.sendAlert();
			}
			
			$scope.messageTemplateSelected = function (selected) {
				const idx = $scope.messageTemplateNames.indexOf(selected);
				$scope.alert = messageTemplates[idx];
				$scope.alert.identifier = 'pending';
				$scope.alert.lang = "en-us";
				
				$scope.info = $scope.alert.infos[0];
				
				$scope.info.pushArea($scope.area);
				$scope.alert.pushInfo($scope.info);
			};
			
			$scope.areaTemplateSelected = function (selected) {
				
				const index = $scope.areaTemplateNames.indexOf(selected);
				$scope.area = areaTemplates[index].infos[0].areas[0];
				
				for (let i = 0; i < $scope.area.polygons; i++) {
					addCapPolygonToMap($scope.area.polygons[i], $scope.area.areaDesc);
				}
				for (let i = 0; i < $scope.area.circles; i++) {
					addCapPolygonToMap($scope.area.circles[i], $scope.area.areaDesc);
				}
				
				// $($scope.area.polygons).each(function(index, value) {
				//   addCapPolygonToMap(value, area.areaDesc);
				// });
				// // clear and reload circles in map
				// $($scope.area.circles).each(function(index, value) {
				//   addCapCircleToMap(value, area.areaDesc);
				// });
				
				
				$scope.info.pushArea($scope.area);
				$scope.alert.identifier = 'pending';
				$scope.alert.lang = "en-us";
			};
			
			function displayMessage(message, type) {
				if (!type) type = "success";
				swal("Creating Alert", message, type);
				
			}
			
			// submit alert JSON to server
			$scope.sendAlert = function () {
				console.log("Attempting to Retrieve Information from Form");
				getInfo();
				
				$scope.sending = true;
				
				let result_message = '';
				$scope.alert.sender =
				console.log($scope.alert.getCAP());
				
				const a_data = {
					'uid': $scope.uid,
					'password': $scope.password,
					'xml': $scope.alert.getCAP()
				};
				
				console.log("Attempting to Send");
				console.log(a_data);
				
				CapService.sendAlert(a_data).then(function (res) {
					console.log(res);
					$scope.sending = false;
					if (res.status === 200) {
						const data = res.data;
						if (data.valid) {
							result_message += 'Success: Valid CAP 1.2 MESSAGE SENT' + '<br>\n';
							
						} else {
							result_message += 'INVALID CAP 1.2' + '<br>\n' + 'SERVER MESSAGE' + ': ' + data.error + '\n';
						}
						const result_uuid = 'UUID: ' + data.uuid;
						// Display the result.
						$scope.responseStatus = result_message;
						$scope.responseId = result_uuid;
						
						$scope.showAlerts = true;
						
						createAlert();
						loadAlerts();
					}
					else if (res.status === 400) {
						result_message = 'Please enter valid login and password.';
						$scope.responseStatus = result_message;
					}
					else if (res.status === 403) {
						result_message = 'You are not authorized to release alerts. ' +
							'Ask your app administrator to be added to the ' +
							'"can release alerts" group.';
						$scope.responseStatus = result_message;
					}
					else {
						result_message = 'POSSIBLE ERROR IN TRANSMISSION. ' + 'Check active alerts before resending.';
						// Display the results.
						$scope.responseStatus = result_message;
					}
				}, function (err) {
					console.log(err);
					$scope.sending = false;
				});
			};
			
			$scope.addGeocode = function () {
				$scope.area.geocodes.push({"valueName": "", "value": ""});
			};
			
			$scope.addParameter = function () {
				$scope.info.parameters.push({"valueName": "", "value": ""});
			};
			
			$scope.addCoord = function () {
				$scope.coords.push({"lat": "", "lng": ""});
			};
			
			$scope.expireChanged = function () {
				$scope.otherSelected = $scope.expires === "other";
			};
			
			// update model with values from screen
			var getInfo = function () {
				$scope.alert.sent = moment().format();
				$scope.sender = $scope.currentUser.fname + " " + $scope.currentUser.lname;
				$scope.alert.source = escape_text($scope.sender);
				
				$scope.info.categories = [];
				$scope.info.addCategory($scope.category || 'Other');
				
				
				$scope.info.senderName = $scope.currentUser.fname + " " + $scope.currentUser.lname;
				
				$scope.info.responseTypes = [];
				// Set default response type if not set.
				$scope.info.addResponseType($scope.responseTypes || 'None');
				
				let expiresInMinutes = $scope.expires;
				
				if (expiresInMinutes === 'other') {
					expiresInMinutes = $scope.expiresOther;
				}
				
				$scope.info.expires = moment().add(expiresInMinutes, 'm').format();
				$scope.info.audience = $scope.currentUser.country;
				
				$scope.area.circles = coordsToCircles().concat(getCircles());
			};
			
			function coordsToCircles() {
				const circles = [];
				for (let i = 0; i < $scope.coords.length; i++) {
					circles.push($scope.coords[i].lat + "," + $scope.coords[i].lng + " " + COORDINATE_RADIUS);
				}
				return circles;
			}
			
			$scope.$watch('coords', function (newValue, oldValue) {
				$scope.area.circles = coordsToCircles().concat(getCircles());
			}, true);
			
			OpenLayers.ImgPath = config.OpenLayersImgPath;
			const Geographic = new OpenLayers.Projection('EPSG:4326');
			const Mercator = new OpenLayers.Projection('EPSG:3857');  // more modern (and official) version of 900913
			
			let map, drawControls, cap_area;
			
			if (!map) {  // only initialize the map once!
				
				if (!cap_area) {
					cap_area = new Area('Undesignated Area');  // constructor in caplib.js
				}
				
				map = new OpenLayers.Map({
					div: 'map',
					allOverlays: true,
					projection: Mercator,
					displayProjection: Geographic,
					autoUpdateSize: true,
					controls: [
						new OpenLayers.Control.Navigation({zoomWheelEnabled: false}),
						new OpenLayers.Control.PanPanel(),
						new OpenLayers.Control.ZoomPanel(),
						new OpenLayers.Control.ArgParser(),
						new OpenLayers.Control.Attribution()
					]
				});
				// create reference layers.
				
				const osmLayer = new OpenLayers.Layer.OSM('OpenStreetMap');
				osmLayer.setVisibility(true);
				osmLayer.setIsBaseLayer(true);
				
				var circleLayer = new OpenLayers.Layer.Vector("Circle Layer");
				circleLayer.events.on({
					"featureadded": function (e) {
						features.push(e.feature);
						featureTypes.push("circle");
						$scope.area.circles = $scope.area.circles.concat(getCircles());
						console.log($scope.area.circles);
					}
				});
				
				var polyLayer = new OpenLayers.Layer.Vector('Drawing Layer');
				polyLayer.events.on({
					"featureadded": function (e) {
						features.push(e.feature);
						featureTypes.push("poly");
						$scope.area.polygons = getPolygons();
					}
				});
				
				// add all layers to map
				map.addLayers([osmLayer, polyLayer, circleLayer]);
				map.setBaseLayer(osmLayer);
				
				// create draw controls
				drawControls = {
					circle: new OpenLayers.Control.DrawFeature(
						circleLayer,
						OpenLayers.Handler.RegularPolygon,
						{
							handlerOptions: {
								sides: 40
							}
						}
					),
					polygon: new OpenLayers.Control.DrawFeature(
						polyLayer,
						OpenLayers.Handler.Polygon
					)
				};
				
				// prevent map from panning when in "circle" mode
				drawControls.circle.handler.stopDown = stop;
				drawControls.circle.handler.stopUp = stop;
				
				// add controls to the map
				// ...including the collection of (named) draw controls
				for (var key in drawControls) {
					if (drawControls.hasOwnProperty(key))
						map.addControl(drawControls[key]);
				}
				map.addControl(new OpenLayers.Control.LayerSwitcher());
				
				// Default map viewport.
				setView(10.4, -61.2, 9);
			}

			// set the map view
			function setView(centerLat, centerLon, zoomLevel) {
				const centerCoords = new OpenLayers.LonLat(centerLon, centerLat);
				map.setCenter(centerCoords.transform(Geographic, Mercator), zoomLevel);
			}

			// handler for radio buttons, activates the corresponding OpenLayers control
			$scope.toggleControl = function (mode) {
				for (key in drawControls) {
					const control = drawControls[key];
					if (mode === key) {
						control.activate();
					} else {
						control.deactivate();
					}
				}
			};

			// return an array of polygon strings
			function getPolygons() {
				const polygonsXML = [];
				if (polyLayer) {
					for (let i = 0; i < polyLayer.features.length; i++) {
						polygonsXML.push(polygonToCapXml(polyLayer.features[i]));
					}
				}
				return polygonsXML;
			}

			// return an array of circle strings
			function getCircles() {
				const circlesXML = [];
				if (circleLayer) {
					for (let i = 0; i < circleLayer.features.length; i++) {
						circlesXML.push(circleToCapXml(circleLayer.features[i]));
					}
				}
				return circlesXML;
			}

			// clear all features drawn to the draw layer
			$scope.clearAllDrawnAreas = function () {
				circleLayer.removeAllFeatures();
				polyLayer.removeAllFeatures();
				$scope.area.circles = coordsToCircles();
				$scope.area.polygons = [];
			};
			
			// remove the last feature added to the draw layer, skipping geocode previews
			$scope.clearLastDrawnArea = function () {
				if (features.length > 0) {
					const type = featureTypes.pop();
					if (type === "circle") {
						circleLayer.removeFeatures(features.pop());
						$scope.area.circles.pop();
					}
					else {
						polyLayer.removeFeatures(features.pop());
						$scope.area.polygons.pop();
					}
				}
			};
			
			// add a polygon in CAP string format as a feature on the drawing layer
			function addCapPolygonToMap(polygonString, source, opt_id) {
				const points = [];
				const pointStrings = polygonString.split(' ');
				// note swap of coordinate order 'twixt CAP and OpenLayers
				for (let i = 0; i < pointStrings.length; i++) {
					const coords = pointStrings[i].split(',');
					points.push(new OpenLayers.Geometry.Point(
						parseFloat(coords[1]),
						parseFloat(coords[0])).transform(Geographic, Mercator));
				}
				const ring = new OpenLayers.Geometry.LinearRing(points);
				const polygon = new OpenLayers.Geometry.Polygon(ring);
				const feature = new OpenLayers.Feature.Vector(polygon);
				feature.attributes = {polygon: true, source: source};
				if (opt_id) {
					feature.attributes.id = opt_id;
				}
				polyLayer.addFeatures([feature]);
			}

			//add a circle in CAP string format as a feature on the drawing layer
			function addCapCircleToMap(circleString, source) {
				const parts = circleString.split(' ');
				const radius = parseFloat(parts[1]) * 1000;
				const coords = parts[0].split(',');
				const centerPoint = new OpenLayers.Geometry.Point(
					parseFloat(coords[1]),
					parseFloat(coords[0])).transform(Geographic, Mercator);
				const circle = new OpenLayers.Geometry.Polygon.createRegularPolygon(
					centerPoint,
					radius,
					40);
				const feature = new OpenLayers.Feature.Vector(circle);
				feature.attributes = {circle: true, source: source};
				circleLayer.addFeatures([feature]);
			}
			
			/**
			 VARIOUS UTILITY FUNCTIONS
			 **/
			
			function polygonToCapXml(feature) {
				const vertices = feature.geometry.getVertices();
				if (vertices.length < 3) return null;  // need at least three vertices
				const polygon = [];
				let polygonString = '';
				for (let i = 0; i < vertices.length; i++) {
					polygonString += pointToRoundedCAPString(vertices[i], 5) + ' ';
				}
				polygonString += pointToRoundedCAPString(vertices[0], 5);
				return polygonString;
			}
			
			function circleToCapXml(circle) {
				const centroid = circle.geometry.getCentroid();
				const radius = radiusOfCircle(circle.geometry);
				//geographicCentroid = centroid.transform(Mercator, Geographic);
				const circleString = pointToRoundedCAPString(centroid, 5) + ' ' + radius;
				return circleString;
				
			}

			// note that this returns the coordinates in CAP (lat,lon) order
			function pointToRoundedCAPString(vertex, decimalPoints) {
				const geo_point = vertex.transform(Mercator, Geographic);
				const string = geo_point.y.toFixed(decimalPoints) + ',' +
					geo_point.x.toFixed(decimalPoints);
				vertex.transform(Geographic, Mercator);  // gotta undo the transform!
				return string;
			}
			
			function radiusOfCircle(circle) {
				const vertices = circle.getVertices();
				const centroid = circle.getCentroid();
				const edgePoint = vertices[0];
				const radius = distanceBetweenPoints(centroid, edgePoint);
				return radius;
			}

			// both points in Mercator projection
			function distanceBetweenPoints(point1, point2) {
				return (point1.distanceTo(point2) / 1000).toFixed(5);  // in km, geodesic
			}
			
			function parseTemplateToAlert(template_xml) {
				const xml = $.parseXML(template_xml);
				const alert = parseCAP2Alert(xml);
				// Non-CAP-compliant fields:
				alert.expiresDurationMinutes = $(xml).find('expiresDurationMinutes').text();
				return alert;
			}
			
			function escape_text(rawText) {
				return $('<div/>').text(rawText).html();
			}
			
		}]);