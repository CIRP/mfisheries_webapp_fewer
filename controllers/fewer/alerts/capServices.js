'use strict';

angular.module('mfisheries.CAP')

  .service("CapService", function($http){

    var CAPCollectorBaseURL = "https://fewercapdemo.herokuapp.com/"; // TODO Set Via Interface

    this.setBaseURL = function(url){
      console.log(url.split("."));
      if (url.split(".").includes("json")) {
	      url = url.split("/").slice(0, -1).join("/") + "/";
      }
      CAPCollectorBaseURL = url;
    };
    
    this.getBaseURL = function(){
      return CAPCollectorBaseURL;
    };
    
    this.getConfig = function( ) {
      var config = {};
      config.CAPCollectorBaseURL = CAPCollectorBaseURL;
      /* Relative path to the "img" subdirectory of OpenLayers install. */
      config.OpenLayersImgPath = CAPCollectorBaseURL + "client/img/";
      config.mapDefaultViewport = "";
      config.defaultExpiresDurationMinutes = 10;
      config.maxHeadlineLength = 140;
      config.versionID = "0.9.3";
      config.useDatetimePicker = true;
      config.timeZone = new Date().getTimezoneOffset();
      return config;
    };

    this.sendAlert = function(data){
      console.log("Attempting to send alert to URL: " + CAPCollectorBaseURL);
      return $http.post(CAPCollectorBaseURL + 'post/', data);
    };

    this.getAlerts = function(){
        return $http.get(CAPCollectorBaseURL + 'feed.json');
    };

    this.getMessageTemplates = function(){
        return $http.get(CAPCollectorBaseURL + 'template/message/');
    };

    this.getAreaTemplates = function(){
        return $http.get(CAPCollectorBaseURL + 'template/area/');
    };
  });
