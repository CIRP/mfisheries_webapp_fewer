"use strict";

/*
class EmgContactCtrl{
	constructor(LocalStorage, fileupload, Country, Module, CountryModule, EmergencyContact, AuthService, $location){
		// Set the Properties of the class
		this.AuthService = AuthService;
		this.Country = Country;
		this.LocalStorage = LocalStorage;
		this.fileupload = fileupload;
		this.Module = Module;
		this.CountryModule = CountryModule;
		this.$location = $location;
		
		
		this.init();
	}
	
	init(){
		this.init();
		
		this.default_pics = {
			'individual': this.$location.protocol() + "://" + window.location.host + "/static/img/emergency/personal_default_profile.png",
			'organization': this.$location.protocol() + "://" + window.location.host + "/static/img/emergency/organization_default_profile.png"
		};
	}
	
	initMenu(){
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
	}
}
*/

angular.module('mfisheries.EmergencyModule', [])
	.controller('adminEmergencyContactCtrl', [
		'$scope', 'LocalStorage', 'fileupload', 'Country', 'Module', 'CountryModule', 'EmergencyContact', 'AuthService', '$location',
		function ($scope, LocalStorage, fileupload, Country, Module, CountryModule, EmergencyContact, AuthService, $location) {
			
			$(".mFish_menu").removeClass("active");
			$("#menu-fewer").addClass("active");
			
			const default_pics = {
				'individual': $location.protocol() + "://" + window.location.host + "/static/img/emergency/personal_default_profile.png",
				'organization': $location.protocol() + "://" + window.location.host + "/static/img/emergency/organization_default_profile.png"
			};
			
			AuthService.attachCurrUser($scope);
			const currUser = $scope.currentUser;
			console.log(currUser);
			
			$scope.phoneExpression = new RegExp('^([0-9]{3})([-. ]?([0-9]{4}))?$');
			$scope.loading = true;
			$scope.emergencyContacts = [];
			$scope.countries = [];
			
			const tableId = "emgTble";
			
			function loadCountries() {
				$scope.country_loading = true;
				Country.get($scope.userCountry).then(function (res) {
					$scope.countries = res.data;
					$scope.country_loading = false;
				});
			}
			
			function loadEmergencyContacts() {
				$scope.loading = true;
				
				EmergencyContact.get($scope.userCountry).then(function (res) {
					console.log("Retrieving Data");
					$scope.emergencyContacts = res.data.map(el => {
						if (el.additional && el.additional.length > 1) {
							el.additionalJSON = JSON.parse(el.additional);
						} else {
							el.additionalJSON = [];
						}
						return el;
					});
					$scope.loading = false;
				});
			}
			
			function resetEmergencyContact(){
				console.log("Resetting emergency contact. Current user is:");
				console.log($scope.currentUser);
				
				let countryid = currUser.countryid || 0;
				let user_id = currUser.userid || 0;
				
				$scope.emergencyContact = {
					'name': '',
					'phone': '',
					'email': '',
					'type': 'organization',
					'additional': '',
					'additionalJSON': [],
					'countryid': countryid,
					'createdby': user_id,
					'image_url': default_pics.organization
				};
				
				resetAddField();
				return $scope.emergencyContact;
			}
			
			function resetAddField() {
				$scope.field = {
					key: '', value: ''
				};
				return $scope.field;
			}
			
			$scope.resetAddField = resetAddField;
			
			$scope.displayNewContactModal = function () {
				resetEmergencyContact();
				$('#newEmergencyContactModal').modal('show');
			};
			
			$scope.displayUpdateContactModal = function (contact) {
				$scope.emergencyContact = contact;
				$('#newEmergencyContactModal').modal('show');
			};
			
			$scope.getNumAddDetails = function (contact) {
				let numDetails = 0;
				if (contact.additionalJSON.length > 0) {
					numDetails = contact.additionalJSON.length;
				}
				return numDetails;
			};
			
			$scope.displayAddDetails = function (contact) {
				console.log("Displaying additional details for: ");
				$scope.emergencyContact = contact;
				console.log($scope.emergencyContact);
				//	TODO Complete the form to view additional information for contact
				$('#addDetailsModal').modal('show');
			};
			
			$scope.saveAdditionalItem = function (field) {
				if (field.key.length >= 3 && field.value.length >= 3) {
					$scope.emergencyContact.additionalJSON.push(field);
					resetAddField();
				} else {
					swal("Add Fields", "Keys and Values must be at least 3 characters long", "error");
				}
			};
			
			$scope.showAdditionalItemForm = function () {
				$("#additional_contact").show('slow');
				$("#additional_contact_btn").show('fast');
			};
			
			$scope.removeAdditionalItem = function (field, i) {
				if ($scope.emergencyContact.additional.length >= i) {
					$scope.emergencyContact.additional.splice(i, 1);
				}
			};
			
			function translateAdditional(contact) {
				// Translate the additional fields to string
				if (contact.additionalJSON.length > 0) {
					contact.additional = JSON.stringify(contact.additionalJSON);
				} else {
					contact.additional = "";
				}
				return contact;
			}
			
			$scope.saveEmergencyContact = function () {
				// TODO Evaluate how this is handled during during an update
				if ($scope.emergencyContact.phone.length === 8) {
					let tempString = $scope.emergencyContact.phone;
					tempString = tempString.slice(0, 3) + tempString.slice(4);
					$scope.emergencyContact.phone = tempString;
				}
				
				$scope.emergencyContact = translateAdditional($scope.emergencyContact);
				
				if ($scope.emergencyContact.id){ // Updating Record
					console.log("Updating the contact: " + JSON.stringify($scope.emergencyContact));
					EmergencyContact.update($scope.emergencyContact).then(function (res) {
						console.log('Updated Emergency Contact');
						if (res.status === 200) {
							$('#newEmergencyContactModal').modal('hide');
							swal('Updating Emergency Contact', 'Successfully Updated Emergency Contact Data', 'success');
						}
						else {
							swal('Updating Emergency Contact', 'Error Updating Emergency Contact Data', 'error');
						}
					});
				}else{ // Saving record
					
					// Ensuring no duplicate Emergency Contact
					let sim = $scope.emergencyContacts.filter((el) => {
						return el.name.toLowerCase() === $scope.emergencyContact.name.toLowerCase() &&
							el.countryid === $scope.emergencyContact.countryid;
					});
					
					if (sim.length < 1){
						console.log("Saving the contact: " + JSON.stringify($scope.emergencyContact));
						EmergencyContact.add($scope.emergencyContact).then(function (res) {
							console.log("Adding the contact: " + JSON.stringify($scope.emergencyContact));
							if (res.status === 201) {
								console.log("Emergency Contact was saved successfully");
								// Add the record from the server to the list
								$scope.emergencyContacts.push(res.data);
								$('#newEmergencyContactModal').modal('hide');
								swal('Add New Emergency Contact', 'Emergency Contact Successfully Added', 'success');
								resetEmergencyContact();
							}
							else {
								swal('Add New Emergency Contact', 'Unable to Save Contact. Ensure all fields are specified correctly', 'error');
							}
						}, function(err){
							console.log(err);
							swal('Add New Emergency Contact', 'Unable to Save Contact. If problem persists, contact administrator', 'error');
						});
					}else{
						swal('Add New Emergency Contact', 'Unable to save contact, contact exists.', 'error');
					}
					
					
				}
				
				
				
			};
			
			$scope.deleteEmergencyContact = function (contact) {
				console.log("Deleting the contact: " + JSON.stringify(contact));
				
				swal({
					title: "Delete Confirmation",
					text: "Are you sure you want to delete this record. You will not be able to undo this operation",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete",
					closeOnConfirm: false
				}).then(result => {
					console.log(result);
					if (result.value) {
						EmergencyContact.delete(contact).then(function (res) {
							console.log('Deleting Emergency Contact');
							if (res.status === 200) {
								swal('Deleting Emergency Contact', 'Emergency Contact Successfully Deleted', 'success');
								loadEmergencyContacts();
							}
							else {
								swal('Deleting Emergency Contact', 'Error Deleting Contact', 'error');
							}
						});
					}
				});
			};
			
			// Procedure for initializing the emergency contact page
			loadEmergencyContacts();
			resetEmergencyContact();
			loadCountries();
			
		}]);