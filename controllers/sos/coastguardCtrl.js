"use strict";

angular.module('mfisheries.CGControllers',[])

.controller('CoastguardCtrl', function($scope, $q, $map, $window, $routeParams, LocalStorage,
                                       getTrackDates, getTracksByDay, resolveSos, USER_ROLES, AuthService){
	$(".mFish_menu").removeClass("active");
	$("#menu-user").addClass("active");
	console.log("Tracks controller loaded");
	
	AuthService.attachCurrUser($scope);
	const currUser = $scope.currentUser;

	const map = $("#map"),
		calendar = $('#calendar');
	let userid;
	const dates = [],
		timer = null;

	$scope.sos = {};

	// Retrieve the URL from the parameter
	if ($routeParams.userid){
		
		// Ensure that only the administrative user can look for specific userid
		if (currUser.userRole === USER_ROLES.admin.code){
			// console.log("Detected that administrative user is present");
			userid = $routeParams.userid;
		}
		console.log("Requesting track information for " + userid);
	}

	// Is defined within the mapService.js file
	$map.initMap(map);
	$map.setType("tracks");

	// Retrieves only the dates where tracks are present
	getTrackDates.get(userid).then(function(res){
		// Update the calendar with the following parameters
		calendar.fullCalendar({
			header: {             //Controls what content should be displayed to the top of the calendar
	      left  : 'prev,next ',
	      center: 'title',
	      right : 'today'
	    },
      events 			  : res.data.data, //the data that will be displayed on calendar in the format: http://fullcalendar.io/docs/event_data/events_array/
      eventColor		: '#378006',
      eventClick		: eventClick
		});

		/**
		The code below fixed a bug, when the calendar renders, the buttons
		display is sent to none, not sure if it is an issue with angular and
		fullcalendar...
	**/
		const d = new Date(),
			data = {
				"day": d.getDate(),
				"month": d.getMonth() + 1,
				"year": d.getFullYear()
			};

		$map.getMarkers(data, true);

	});

	function eventClick(calEvent){
		const s = calEvent.start._i.split('-'),
			d = new Date(parseInt(s[0]), parseInt(s[1]) - 1, parseInt(s[2])),
			today = new Date(),
			data = {
				"day": d.getDate(),
				"month": d.getMonth() + 1,
				"year": d.getFullYear()
			};


		if(d.getDate() === today.getDate() && d.getMonth() === today.getMonth()){
			console.log('something');
			clearInterval($map.timer);
			$map.getMarkers(data, true);
		}else{
			$map.getMarkers(data, false);
			clearInterval($map.timer);
		}

		$map.zoomOut();
	}


	$scope.hideshow = function(){
		console.log("Clicked");

		const sidebar = $('#sidebar'),
			main = $('#main'),
			button = $('#sidebar_button').find('span'),
			map = $("#map"),
			main_class_lg = 'col-md-12 col-lg-12',
			
			main_class_sm = 'col-md-8 col-lg-8',
			sidebar_class = 'col-md-4 col-lg-4';

		if($(main).hasClass(main_class_lg)){

			$(main).removeClass(main_class_lg);
			$(main).addClass(main_class_sm);

			$(button).removeClass('glyphicon glyphicon-chevron-right');
			$(button).addClass('glyphicon glyphicon-chevron-left');

			$(sidebar).addClass(sidebar_class);
			$(sidebar).show();

		}else{
			$(sidebar).hide();
			$(sidebar).removeClass(sidebar_class);

			$(button).removeClass('glyphicon glyphicon-chevron-left');
			$(button).addClass('glyphicon glyphicon-chevron-right');

			$(main).removeClass(main_class_sm);
			$(main).addClass(main_class_lg);
		}
		resizeMap();
	};

	function resizeMap(){
		const center = $map.map.getCenter();
	 	google.maps.event.trigger($map.map, "resize");
	 	$map.map.setCenter(center);
	 	console.log('resized');
	}

	function getTimeStamp  () {
       const now = new Date();
       return (now.getFullYear() + '-' + (now.getDate()) + '-' + (now.getMonth() + 1) + " " + now.getHours() + ':' +
                      ((now.getMinutes() < 10) ? ("0" + now.getMinutes()) : (now.getMinutes())) + ':' + ((now.getSeconds() < 10) ? ("0" + now
		                     .getSeconds()) : (now.getSeconds())));
	}

	$('#myModal').on('shown.bs.modal', function (event) {
	  const button = $(event.relatedTarget);
		console.log(button[0]);
	  $scope.sos = {};
	  $scope.sos.trackid = button[0].dataset.id;
	  $scope.sos.username = button[0].dataset.name;
	  $scope.sos.date = new Date();
	  $scope.sos.lat = button[0].dataset.lat;
	  $scope.sos.lng = button[0].dataset.lng;
	  $scope.sos.caseno = "";
	  $scope.sos.badgeno = "";
	  $scope.sos.details = "";
		$scope.$apply();
	}).on('hidden.bs.modal', function (event) { });

	$scope.resolveSosStatus = function(){
		console.log("Attempting to submit resolution to sos");
		let valid = true;
		$.each($scope.sos, function(index, data){
			if(index === "caseno" && data === ""){
				$("#caseno").parent().addClass("has-error");
				valid = false;
			}
			if(index === "badgeno" && data === ""){
				$("#badgeno").parent().addClass("has-error");
				valid = false;
			}
			if(index === "details" && data === ""){
				$("#details").parent().addClass("has-error");
				valid = false;
			}
			console.log($map.markers);
		});
		if(valid){

			$("#myModal").modal('hide')
				.on('hidden.bs.modal',
					function(event){
						// Delete date (for invoking automatic timestamp on the server)
						delete $scope.sos.date;
						console.log("Sending the Resolve SOS to the server: " + $scope.sos);
						// Submit to the server as an update for the SOS
						const resolve = resolveSos.post($scope.sos);
						resolve.then(function(res){
							if (res.data.status === 200){
								swal("SOS", "SOS was resolved successfully!", "success");
								$map.removeMarker($scope.sos.lat, $scope.sos.lng);
							}
						},
						function(res){
							console.log(res);
							swal("SOS", "Unable to resolve SOS!", "error");
						});
					}
				);
		}

	};

});
