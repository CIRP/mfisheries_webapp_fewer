// angular.module('mfisheries.Weather')
// 	.service("Threshold", ["$http", function ($http) {
// 		var weather = {};
//
// 		weather.get = function (id) {
// 			if (id && id !== 0){
// 			return $http.get('/api/weather/thresholds/'+ id);
// 			}
// 			return $http.get('/api/weather/thresholds');
// 		};
//
// 		weather.add = function (data) {
// 			return $http.post('/api/weather/thresholds', data);
// 		};
//
// 		weather.delete = function (index) {
// 			return $http.delete('/api/weather/thresholds/' + index);
// 		};
//
// 		weather.update = function(source){
// 			return $http.put('/api/weather/thresholds/' + source.weathersourceid, source);
// 		};
// 		return weather;
// 	}]);

class Threshold extends BaseAPIService{
	/**
	 *
	 * @param $http
	 */
	constructor($http){
		super($http, "/api/weather/thresholds");
	}
}

Threshold.$inject = [
	"$http"
];
angular.module('mfisheries.Weather').service("Threshold", Threshold);