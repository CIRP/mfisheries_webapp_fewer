// angular.module('mfisheries.AlertGroupController')
// .service("Group",["$http", function($http){
// 	let group = {};
//
// 	group.get = function(countryid){
// 		if (countryid && countryid !== 0)
// 			return $http.get('/api/groups', {
// 				params: {
// 					"countryid": countryid ,
// 					"expanded" : true
// 				}
// 			});
// 		return $http.get('/api/groups');
// 	};
//
// 	group.add = function(group){
// 		return $http.post('/api/groups', group);
// 	};
//
// 	group.update = function(group){
// 		return $http.put('/api/groups/' + group.id, group);
// 	};
//
// 	group.delete = function(id){
// 		return $http.delete('/api/groups/'+id);
// 	};
//
// 	return group;
// }]);

class Group extends BaseAPIService{
	/**
	 *
	 * @param $http
	 */
	constructor($http){
		super($http, "/api/groups");
	}
}

Group.$inject = [
	"$http"
];
angular.module('mfisheries.AlertGroupController').service("Group", Group);