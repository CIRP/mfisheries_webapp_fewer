'use strict';

angular.module('mfisheries.MapHelperServices', [])
	
	.service("mapHelper", function () {
		const self = this;
		
		this.dec2StrLat = function (decLatitude) {
			let intDegree,
				decMinute,
				strLatitude = "N";
			if (decLatitude < 0) {
				strLatitude = "S";
				decLatitude = decLatitude * -1;
			}
			intDegree = Math.floor(decLatitude);
			decMinute = (decLatitude - intDegree) * 60;
			decMinute = self.myRound(decMinute, 3);
			strLatitude = String(intDegree) + "° " + String(decMinute) + "' " + strLatitude;
			return strLatitude;
		};
		
		this.dec2StrLng = function (decLongitude) {
			let intDegree,
				decMinute,
				strLongitude = "E";
			if (decLongitude < 0) {
				strLongitude = "W";
				decLongitude = decLongitude * -1;
			}
			intDegree = Math.floor(decLongitude);
			decMinute = (decLongitude - intDegree) * 60;
			decMinute = self.myRound(decMinute, 3);
			strLongitude = String(intDegree) + "° " + String(decMinute) + "'' " + strLongitude;
			return strLongitude;
		};
		
		this.myRound = function (zahl, n) {
			let faktor;
			faktor = Math.pow(10, n);
			return (Math.round(zahl * faktor) / faktor);
		};
		
		this.trackWindow = function (data) {
			const lat = self.dec2StrLat(data.latitude),
				lng = self.dec2StrLng(data.longitude);
				return $('<div class="form" role="form">' +
					'<div class="form-group">' +
					'<label style = "font-weight: 100;" class="control-label">User:</label>' +
					'<div>' + data.userid + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">GPS:</label>' +
					'<div>' + lat + ', ' + lng + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">Time:</label>' +
					'<div>' + data.time + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">Bearing:</label>' +
					'<div>' + data.bearing + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">Speed:</label>' +
					'<div>' + data.speed + 'm/s</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<button id = "' + data.userid + '" class = "getuser btn btn-default"' +
					'data-day = "' + data.day + '" data-month = "' + data.month + '" data-year = "' + data.year + '" data-starttrackid = "' + data.starttrackid + '">All Tracks</button>' +
					// '<button id = "'+data.userid+'_r" class = "getuser btn btn-default"'+
					// 'data-day = "'+data.day+'" data-month = "'+data.month+'">Replay</button>'+
					'</div>' + '</div>'
				);
		};
		
		this.sosWindow = function (data) {
			const lat = self.dec2StrLat(data.latitude),
				lng = self.dec2StrLng(data.longitude);
			
				return $('<div class="form" role="form">' +
					'<div class="form-group">' +
					'<label style = "font-weight: 100;" class="control-label">User:</label>' +
					'<div>' + data.fname + ' ' + data.lname + ' </div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label style = "font-weight: 100;" class="control-label">Contact:</label>' +
					'<div>' + data.mobileNum + ' </div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">GPS:</label>' +
					'<div>' + lat + ', ' + lng + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">Time:</label>' +
					'<div>' + data.time + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">Bearing:</label>' +
					'<div>' + data.bearing + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">Speed:</label>' +
					'<div>' + data.speed + 'm/s</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<button class = "getuser btn btn-default" data-day = "' +
						data.day + '" data-month = "' + data.month + '" data-year = "' + data.year + '" data-starttrackid = "' + data.starttrackid +
						'">All Tracks</button>' +
					'<button class = "getuser btn btn-default sos-modal" data-id = "' + data.id + '" data-toggle="modal" data-target="#myModal" data-name = "' +
						data.fname + ' ' + data.lname + '" data-lat = "' + data.latitude + '" data-lng = "' + data.longitude + '">Resolve</button>' +
					'</div>' + '</div>'
				);
		};
		
		this.allTrackWindow = function (data) {
			const lat = self.dec2StrLat(data.latitude),
				lng = self.dec2StrLng(data.longitude);
			
				return $('<div class="form" role="form">' +
					'<div class="form-group">' +
					'<label style = "font-weight: 100;" class="control-label">User:</label>' +
					'<div>' + data.userid + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">GPS:</label>' +
					'<div>' + lat + ', ' + lng + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">Time:</label>' +
					'<div>' + data.time + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">Bearing:</label>' +
					'<div>' + data.bearing + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">Speed:</label>' +
					'<div>' + data.speed + 'm/s</div>' +
					'</div>' +
					'</div>'
				);
		};
		
		this.allSosWindow = function (data) {
			console.log("Inside All SOS Window");
			const lat = self.dec2StrLat(data.latitude),
				lng = self.dec2StrLng(data.longitude);
			
			return $('<div class="form" role="form">' +
				'<div class="form-group">' +
				'<label style = "font-weight: 100;" class="control-label">User:</label>' +
				'<div>' + data.fname + ' ' + data.lname + ' </div>' +
				'</div>' +
				'<div class="form-group">' +
				'<label style = "font-weight: 100;" class="control-label">Contact:</label>' +
				'<div>' + data.mobileNum + '</div>' +
				'</div>' +
				'<div class="form-group">' +
				'<label class="control-label">GPS:</label>' +
				'<div>' + lat + ', ' + lng + '</div>' +
				'</div>' +
				'<div class="form-group">' +
				'<label class="control-label">Time:</label>' +
				'<div>' + data.time + '</div>' +
				'</div>' +
				'<div class="form-group">' +
				'<label class="control-label">Bearing:</label>' +
				'<div>' + data.bearing + '</div>' +
				'</div>' +
				'<div class="form-group">' +
				'<label class="control-label">Speed:</label>' +
				'<div>' + data.speed + 'm/s</div>' +
				'</div>' +
				'<button type="button" class = "getuser btn btn-default sos-modal" data-id = "' + data.id +
				'" data-toggle="modal" data-target="#myModal" data-name = "' + data.fname + ' ' + data.lname + '" data-lat = "' + data.latitude + '" data-lng = "' + data.longitude + '">Resolve</button>' +
				'</div>'
			);
		};
		
		this.endTrackWindow = function (data) {
			const lat = self.dec2StrLat(data.latitude),
				lng = self.dec2StrLng(data.longitude);
			
			return $('<div class="form" role="form">' +
				'<div class="form-group">' +
				'<label style = "font-weight: 100;" class="control-label">User:</label>' +
				'<div>' + data.userid + '</div>' +
				'</div>' +
				'<div class="form-group">' +
				'<label class="control-label">GPS:</label>' +
				'<div>' + lat + ', ' + lng + '</div>' +
				'</div>' +
				'<div class="form-group">' +
				'<label class="control-label">Time:</label>' +
				'<div>' + data.time + '</div>' +
				'</div>' +
				'<div class="form-group">' +
				'<label class="control-label">Bearing:</label>' +
				'<div>' + data.bearing + '</div>' +
				'</div>' +
				'<div class="form-group">' +
				'<label class="control-label">Speed:</label>' +
				'<div>' + data.speed + 'm/s</div>' +
				'</div>' +
				'<div class="form-group">' +
				'<button id = "' + data.userid + '" class = "getuser btn btn-default"' +
				'data-starttrackid = "' + data.starttrackid + '" data-timestamp = "' + data.time + '">Get Trip</button>' +
				'<button id = "' + data.userid + '_r" class = "getuser btn btn-default"' +
				'data-starttrackid = "' + data.starttrackid + '" style = "display:none">Toggle Trip</button>' +
				'<label class="control-label no-trip" style = "display:none">No Trips Planned</label>' +
				'</div>' + '</div>'
			);
		};
		
		this.tripWindow = function (data) {
			const lat = self.dec2StrLat(data.latitude),
				lng = self.dec2StrLng(data.longitude);
			
				return $('<div class="form" role="form">' +
					'<div class="form-group">' +
					'<label style = "font-weight: 100;" class="control-label">User:</label>' +
					'<div>' + data.userid + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">GPS:</label>' +
					'<div>' + lat + ', ' + lng + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">Date Started:</label>' +
					'<div>' + data.startdate + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">Expected End:</label>' +
					'<div>' + data.expectedenddate + '</div>' +
					'</div>' +
					'</div>'
				);
		};
		
		this.tripWindow = function (data) {
			const lat = self.dec2StrLat(data.latitude),
				lng = self.dec2StrLng(data.longitude);
			
				return $('<div class="form" role="form">' +
					'<div class="form-group">' +
					'<label style = "font-weight: 100;" class="control-label">User:</label>' +
					'<div>' + data.fname + ' ' + data.lname + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">Contact:</label>' +
					'<div>' + data.phoneNum + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">GPS:</label>' +
					'<div>' + lat + ', ' + lng + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">Date:</label>' +
					'<div>' + data.start + '</div>' +
					'</div>' +
					'</div>'
				);
		};
		
		this.tripWindow = function (data) {
			const lat = self.dec2StrLat(data.latitude),
				lng = self.dec2StrLng(data.longitude);
			
				return $('<div class="form" role="form">' +
					'<div class="form-group">' +
					'<label style = "font-weight: 100;" class="control-label">User:</label>' +
					'<div>' + data.userid + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">GPS:</label>' +
					'<div>' + lat + ', ' + lng + '</div>' +
					'</div>' +
					'<div class="form-group">' +
					'<label class="control-label">Date Started:</label>' +
					'<div>' + data.start + '</div>' +
					'</div>' +
					'</div>'
				);
		};
		
		this.imageWindow = function (data) {
			
			const carousel = $('<div id="carousel-example-generic" class="carousel slide" data-ride="carousel"></div>'),
				carousel_indicators = $('<ol class="carousel-indicators"></ol>'),
				listbox = $('<div class="carousel-inner" role="listbox"></div>'),
				left = $('<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">' +
					'<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>' +
					'<span class="sr-only">Previous</span>' +
					'</a>'),
				right = $('<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">' +
					'<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>' +
					'<span class="sr-only">Next</span>' +
					'</a>');
			
			$.each(data, function (index, d) {
				let li, item;
				if (index === 0) {
					li = $('<li data-target="#carousel-example-generic" data-slide-to="' + index + '" class="active"></li>');
					item = $('<div class="item active">' +
						'<img src=".' + d.imglocation + '" alt="' + d.date + '" style = "height:450px">' +
						'<div class="carousel-caption">' + d.date +
						'</div>');
				} else {
					li = $('<li data-target="#carousel-example-generic" data-slide-to="' + index + '"></li>');
					item = $('<div class="item">' +
						'<img src=".' + d.imglocation + '" alt="' + d.date + '" style = "height:450px">' +
						'<div class="carousel-caption">' + d.date +
						'</div>');
				}
				
				carousel_indicators.append(li);
				listbox.append(item);
				
			});
			
			carousel.append(carousel_indicators);
			carousel.append(listbox);
			carousel.append(left);
			carousel.append(right);
			return carousel;
		};
		
		this.lekWindow = function (data) {
			
			const carousel = $('<div id="carousel-example-generic" class="carousel slide" data-ride="carousel"></div>'),
				carousel_indicators = $('<ol class="carousel-indicators"></ol>'),
				listbox = $('<div class="carousel-inner" role="listbox"></div>'),
				left = $('<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">' +
					'<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>' +
					'<span class="sr-only">Previous</span>' +
					'</a>'),
				right = $('<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">' +
					'<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>' +
					'<span class="sr-only">Next</span>' +
					'</a>');
			
			$.each(data, function (index, d) {
				let li, item;
				if (index === 0) {
					if (d.filetype.includes("image")) {
						li = $('<li data-target="#carousel-example-generic" data-slide-to="' + index + '" class="active"></li>');
						item = $('<div class="item active">' +
								'<img src=".' + d.location + '" alt="' + d.timestamp + '" style = "height:450px">' +
								'<div class="carousel-caption">' + d.timestamp +
								'</div>');
					}
					else if (d.filetype.includes("video")) {
						li = $('<li data-target="#carousel-example-generic" data-slide-to="' + index + '" class="active"></li>');
						item = $('<div class="item">' +
								'<video width="320" height="240" controls><source src="' +
								d.location + '" ng-type="' + d.filetype +
								'">Your browser does not support the video tag.</video>' +
								'</div>');
					}
					else if (d.filetype.includes("audio")) {
						li = $('<li data-target="#carousel-example-generic" data-slide-to="' + index + '" class="active"></li>');
						item = $('<div class="item">' +
								'<audio controls><source src="' +
								d.location + '" ng-type="' + d.filetype +
								'">Your browser does not support the audio element.</audio>' +
								'</div>');
					}
				} else {
					if (d.filetype.includes("image")) {
						li = $('<li data-target="#carousel-example-generic" data-slide-to="' + index + '"></li>');
						item = $('<div class="item">' +
								'<img src=".' + d.location + '" alt="' + d.timestamp + '" style = "height:450px">' +
								'<div class="carousel-caption">' + d.timestamp +
								'</div>');
					}
					else if (data.filetype.includes("video")) {
						li = $('<li data-target="#carousel-example-generic" data-slide-to="' + index + '" ></li>');
						item = $('<div class="item">' +
								'<video width="320" height="240" controls><source src="' +
								d.location + '" ng-type="' + d.filetype +
								'">Your browser does not support the video tag.</video>' +
								'</div>');
					}
					else if (d.filetype.includes("audio")) {
						li = $('<li data-target="#carousel-example-generic" data-slide-to="' + index + '"></li>');
						item = $('<div class="item">' +
								'<audio controls><source src="' +
								d.location + '" ng-type="' + d.filetype +
								'">Your browser does not support the audio element.</audio>' +
								'</div>');
					}
				}
				
				carousel_indicators.append(li);
				listbox.append(item);
				
			});
			
			carousel.append(carousel_indicators);
			carousel.append(listbox);
			carousel.append(left);
			carousel.append(right);
			return carousel;
		};
	});
