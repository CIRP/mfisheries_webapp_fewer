'use strict';

angular.module('mfisheries.services')
	.service('fileUpload', ['$http', function ($http) { // https://uncorkedstudios.com/blog/multipartformdata-file-upload-with-angularjs
		var deffered = {};
		
		deffered.uploadFileToUrl = function (file, data, uploadUrl) {
			var fd = new FormData();
			fd.append('file', file);
			for (var d in data) {
				if (data.hasOwnProperty(d))
					fd.append(d, data[d]);
			}
			return $http.post(uploadUrl, fd, {
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			});
		};
		
		deffered.post = function (data) {
			
			var fd = new FormData();
			for (var key in data) {
				if (data.hasOwnProperty(key))
					fd.append(key, data[key]);
			}
			console.log(data);
			return $http.post('api/add/lek', fd,
				{
					transformRequest: angular.indentity,
					headers: {'Content-Type': undefined}
				});
		};
		
		return deffered;
	}])
	
	.service("login", function ($http) {
		var deffered = {};
		
		deffered.post = function (data) {
			
			return $http.post('api/user/login', data);
		};
		return deffered;
	});