/**
 * A wrapper surrounding the use of a local perstence strategy
 */
class LocalStorage{
	
	/**
	 *
	 * @param $window
	 */
	constructor($window){
		this.$window = $window;
		this.localStorage = $window.localStorage;
	}
	
	/**
	 *
	 * @param key
	 * @param value
	 */
	set(key, value) {
		this.save(key,value);
	}

	/**
	 *
	 * @param key
	 * @param value
	 * @returns {Promise.<{key: *, value: *}>}
	 */
	save(key, value){
		// TODO Implement ability to store object
		this.localStorage.setItem(key,value);
		return Promise.resolve({'key': key, 'value': value});
	}

	/**
	 *
	 * @param key
	 * @param usePromise
	 * @returns {Promise.<T>}
	 */
	get(key, usePromise) {
		const value = this.localStorage.getItem(key);
		if (usePromise){
			return new Promise((resolve, reject) =>{
				if (value)resolve(value);
				else reject(value);
			});
		}
		return value;
	}
	
	/**
	 *
	 * @param key
	 * @param value
	 */
	setObject(key, value) {
		this.localStorage.setItem(key,JSON.stringify(value));
	}
	
	/**
	 *
	 * @param key
	 */
	getObject(key) {
		return JSON.parse(this.$window.localStorage.getItem(key));
	}
	
	/**
	 *
	 * @param key
	 */
	delete(key){
		this.$window.localStorage.removeItem(key);
	}
	
	/**
	 *
	 * @param key
	 * @returns {boolean}
	 */
	hasKey(key){
		return this.$window.localStorage.getItem(key) !== null;
	}
	
	/**
	 *
	 */
	clearCache(){
		this.$window.localStorage.clear();
	}
}

LocalStorage.$inject = [
	"$window"
];

angular.module('mfisheries.services').service("LocalStorage", LocalStorage);