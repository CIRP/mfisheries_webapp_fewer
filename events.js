//TODO: Angular 4 migration consideration: https://stackoverflow.com/questions/34986922/define-global-constants-in-angular-2
//https://angular.io/docs/ts/latest/guide/dependency-injection.html#!#dependency-injection-tokens
angular.module('mfisheries')
	.constant('AUTH_EVENTS', {
		loginSuccess: 'auth-login-success',
		loginFailed: 'auth-login-failed',
		logoutSuccess: 'auth-logout-success',
		sessionTimeout: 'auth-session-timeout',
		notAuthenticated: 'auth-not-authenticated',
		notAuthorized: 'auth-not-authorized',
		newgoogleUser: 'auth-new-googleuser'
	});